
#include "funzioni.h"


void scelta_errata(void)
{
    printf("\n\n La tua scelta non e' valida!\n\n");
}

int menu_principale(void)
{

    int scelta_menu;

    do
    {

        printf("\n MENU PRINCIPALE");
        printf("\n Digita il numero corrispondente a una voce del menu e premi invio:");
        printf("\n 1 Gestisci il menu dei piatti");
        printf("\n 2 Gestisci la cassa e le ordinazioni");
        printf("\n 3 FUNZIONALITA' AVANZATE! Visualizza scorte");
        printf("\n 4 Uscita");

        printf("\n\n Scelta: ");
        scanf("%d", &scelta_menu);
        getchar();
        fflush(stdin);
        if(scelta_menu<1 || scelta_menu>4)
            scelta_errata();

    }

    while(scelta_menu<1 || scelta_menu>4);
    return scelta_menu;

}

int gestione_cassa (void)
{


    int scelta_menu;


    printf("\n GESTIONE CASSA");

    printf("\n Per il tavolo scelto sono disponibili le seguenti opzioni. \n Digita il numero corrispondente a una voce del menu e premi invio:");


    printf("\n 1 Apertura tavolo");
    printf("\n 2 Aggiungi ordine");
    printf("\n 3 Visualizza / stampa scontrino");
    printf("\n 4 Menu principale");

    printf("\n\n Scelta: ");
    scanf("%d", &scelta_menu);
    fflush(stdin);
    getchar();

    return scelta_menu;


}

void carica_file_supporto(int *vettore_di_supporto,int *p_totale_piatti,int *p_contatore_piatti)
{
    FILE *file=NULL;

    file =          fopen ( "supporto.dat" , "rb" ) ;
    fread(vettore_di_supporto , sizeof ( int ) , 2, file ) ;
    fclose( file ) ;

    *p_totale_piatti=*vettore_di_supporto;
    *p_contatore_piatti=*(vettore_di_supporto+1);
}



void carica_file_menu(piatto *p_vettorepiatti, int p_totale_piatti)    //riga 72 vedi errori compilatore
{
    FILE *file=NULL;
    if (p_vettorepiatti == NULL)
    {
        printf ("\nMEMORIA ESAURITA\n");
        exit (-1);
    }
    file = fopen ( "piatti.dat" , "rb" ) ;      //apro il file contenente il menu in modalit lettua (devo solo leggere il menu e caricarlo)

    fread(p_vettorepiatti , sizeof (piatto) , p_totale_piatti, file ) ; //leggo il menu

    fclose(file) ;  //chiudo il file dl menu dopo averlo caricato




}

void riempi_ingredienti (piatto *p_vettorepiatti, int p_totale_piatti)


{
    int i, x;
    for(i=0; i<p_totale_piatti; i++)
    {
        for(x=0; x<30; x++)
        {
            p_vettorepiatti[(p_totale_piatti-1)].nome_piatto[x]=' ';
        }
        for(x=0; x<60; x++)
        {
            p_vettorepiatti[(p_totale_piatti-1)].ingredienti[x]=' ';
        }
    }
}


int controllo_doppioni(piatto *p_vettorepiatti, int p_totale_piatti)
{
    int i;
    //printf(" %s ", p_vettorepiatti[(p_totale_piatti-1)].nome_piatto);
    for(i=0; i<p_totale_piatti-1; i++)
    {

        //printf(" %s ", p_vettorepiatti[i].nome_piatto);
        if(strcmp(p_vettorepiatti[(p_totale_piatti-1)].nome_piatto, p_vettorepiatti[i].nome_piatto)==0)
            return 0;

    }
    return 1;

}

void inserimento_campo_piatto (piatto *p_vettorepiatti, int p_totale_piatti, int campo_piatti)
{

    int scelta_tipo_piatto;


    switch(campo_piatti)
    {
    case 1:
        do
        {
            printf("\nNome: ");
            scanf("%[^\n]s", p_vettorepiatti[(p_totale_piatti-1)].nome_piatto);      //inserimento nome
            getchar();
            //fflush(stdin);
        }
        while(controllo_doppioni(p_vettorepiatti, p_totale_piatti)==0);

        break;

    case 2:

        do
        {
            printf("\nTipologia:  0=antipasto, 1=primo, 2=secondo, 3=contorno, 4=dessert\n");
            scanf("%d", &scelta_tipo_piatto);                                           // inserimento tipo
            getchar();
            fflush(stdin);
            if (scelta_tipo_piatto<0 || scelta_tipo_piatto>4)
                scelta_errata();
        }
        while (scelta_tipo_piatto<0 || scelta_tipo_piatto>4);

        p_vettorepiatti[(p_totale_piatti-1)].tipo_piatto=scelta_tipo_piatto;

        break;

    case 3:

        printf("\nIngrediente1: ");
        scanf("%[^\n]s", p_vettorepiatti[(p_totale_piatti-1)].ingredienti);          //inserimento ingredienti
        getchar();
        printf("\nIngrediente2: ");
        scanf("%[^\n]s", p_vettorepiatti[(p_totale_piatti-1)].ingredienti+20);
        getchar();
        printf("\nIngrediente3: ");
        scanf("%[^\n]s", p_vettorepiatti[(p_totale_piatti-1)].ingredienti+40);
        getchar();

        break;


    case 4:

        do
        {


            printf("\nPrezzo: ");
            scanf("%f", &p_vettorepiatti[(p_totale_piatti-1)].prezzo);           //inserisci prezzo
            fflush(stdin);
        }

        while(p_vettorepiatti[(p_totale_piatti-1)].prezzo<0 || p_vettorepiatti[(p_totale_piatti-1)].prezzo>1000);

        break;
    }
}


void memorizza_file_supporto(int *vettore_di_supporto)
{


    FILE *file=NULL;



    file =          fopen ( "supporto.dat" , "wb" ) ;
    fwrite(vettore_di_supporto , sizeof ( int ) , 2, file ) ;
    fclose( file ) ;
}

void memorizza_file_menu(piatto *p_vettorepiatti, int p_totale_piatti)
{
    FILE *file=NULL;


    file = fopen ( "piatti.dat" , "wb" ) ;
    fwrite(p_vettorepiatti , sizeof ( piatto ) , p_totale_piatti, file ) ;
    fclose( file ) ;
}

void stampa_menu(piatto *p_vettorepiatti, int p_totale_piatti)
{
    int i;
    for(i=0; i<p_totale_piatti; i++)
    {
        printf("\n ID=%d", p_vettorepiatti[i].ID);
        printf(" %s ", p_vettorepiatti[i].nome_piatto);
        switch (p_vettorepiatti[i].tipo_piatto)
        {
        case 0:
            printf("antipasto");
            break;
        case 1:
            printf("primo");
            break;
        case 2:
            printf("secondo");
            break;
        case 3:
            printf("contorno");
            break;
        case 4:
            printf("dessert");
            break;
        }
        printf(" %s, %s, %s ", p_vettorepiatti[i].ingredienti, p_vettorepiatti[i].ingredienti+20, p_vettorepiatti[i].ingredienti+40);
        printf("%.2f", p_vettorepiatti[i].prezzo);
    }
}


int selezione_piatto_e_controllo (int* piatto_selezionato_indice, int contatore_piatti, int p_totale_piatti, piatto* p_vettorepiatti)
{

    int flagcontrollopiatti=0;
    int i;
    int c;


    do
    {


        do
        {

            printf("\n\n Piatto scelto: ");   //scelta
            scanf("%d", &c);
            getchar();
            fflush(stdin);

            if(c<0 || c>=contatore_piatti)
                printf("\n\n Piatto Inesistente! \n\n");
        }
        //FUNZ CONTROLLO ESISTENZA PIATTI
        while(c<0 || c>=contatore_piatti);

        for(i=0; i<p_totale_piatti; i++)        //Secondo controllo utile in caso di piatto eliminato, scorre l'elenco dei piatti alla ricerca del piatto scelto.
            if (c==p_vettorepiatti[i].ID)
            {
                flagcontrollopiatti=1;              // Se lo trova viene memorizzata l'informazione "trovato" in un'apposita variabile flag
                *piatto_selezionato_indice=i;
            }
        if (flagcontrollopiatti==0)
            printf("\n\n Piatto Inesistente! \n\n");
    }
    while(flagcontrollopiatti==0);

    flagcontrollopiatti=0;

    return c;
}


void compattazione_piatti_dopo_eliminazione(int piatto_selezionato_indice, int contatore_piatti, int p_totale_piatti, int c, piatto* p_vettorepiatti)
{
    int i;
    int x;

    if (c<contatore_piatti)
    {

        i=piatto_selezionato_indice;

        for(; i<p_totale_piatti; i++)
        {
            p_vettorepiatti[i].ID=p_vettorepiatti[i+1].ID;
            for(x=0; x<30; x++)
            {
                p_vettorepiatti[i].nome_piatto[x]=p_vettorepiatti[i+1].nome_piatto[x];
            }

            p_vettorepiatti[i].tipo_piatto=p_vettorepiatti[i+1].tipo_piatto;
            for(x=0; x<60; x++)
            {
                p_vettorepiatti[i].ingredienti[x]=p_vettorepiatti[i+1].ingredienti[x];
            }
            p_vettorepiatti[i].prezzo=p_vettorepiatti[i+1].prezzo;
        }

    }


}


int modifica_piatti(int piatto_selezionato_indice, int contatore_piatti, int p_totale_piatti, int c, piatto* p_vettorepiatti)
{

    int sceltacampomod;
    char continuamod;
    int scelta_tipo_piatto;
    // int i;

    if (c<contatore_piatti)
    {
        //for(i=0; i<p_totale_piatti; i++)
        //{
        //  if (p_vettorepiatti[i].ID==c)
        //{

        do
        {

            printf("\n Quale campo vuoi modificare?");    //Scegli campo da modificare
            printf("\n 1 nome \n 2 tipologia \n 3 ingrediente1 \n 4 ingrediente2 \n 5 ingrediente3 \n 6 prezzo\n");
            scanf("%d", &sceltacampomod);
            getchar();
            fflush(stdin);
        }
        while(sceltacampomod<0 || sceltacampomod>6);



        /*Ogni case � relativo a un campo da modificare*/
        switch (sceltacampomod)
        {
        case 1:
            printf("\nnome:");
            scanf("%[^\n]s", p_vettorepiatti[piatto_selezionato_indice].nome_piatto);
            getchar();
            printf("\naltra modifica? y/n");
            scanf("%s", &continuamod);


            break;
        case 2:
            printf("\ntipologia:  0=antipasto, 1=primo, 2=secondo, 3=contorno, 4=dessert");
            scanf("%d", &scelta_tipo_piatto);
            p_vettorepiatti[piatto_selezionato_indice].tipo_piatto=scelta_tipo_piatto;
            getchar();
            printf("\naltra modifica? y/n");
            scanf("%s", &continuamod);



            break;
        case 3:
            printf("\ningrediente1:");
            scanf("%[^\n]s", p_vettorepiatti[piatto_selezionato_indice].ingredienti);
            getchar();
            printf("\naltra modifica? y/n");
            scanf("%s", &continuamod);


            break;
        case 4:
            printf("\ningrediente2:");
            scanf("%[^\n]s", p_vettorepiatti[piatto_selezionato_indice].ingredienti+20);
            getchar();
            printf("\naltra modifica? y/n");
            scanf("%s", &continuamod);


            break;
        case 5:
            printf("\ningrediente3:");
            scanf("%[^\n]s", p_vettorepiatti[piatto_selezionato_indice].ingredienti+40);
            getchar();
            printf("\naltra modifica? y/n");
            scanf("%s", &continuamod);


            break;

        case 6:
            printf("\nprezzo:");
            scanf("%f", &p_vettorepiatti[piatto_selezionato_indice].prezzo);
            getchar();
            printf("\naltra modifica? y/n");
            scanf("%s", &continuamod);

            break;

        }


    }
    return continuamod;
}


void ricerca_per_ingrediente(int p_totale_piatti, piatto* p_vettorepiatti)
{
    char ricercaingrediente[20];
    int i;
    int flagricerca=0;

    printf("\n Cerca il piatto per ingrediente: ");
    scanf("%s", ricercaingrediente );


    for(i=0; i<p_totale_piatti; i++)
        if (strcmp( ricercaingrediente , p_vettorepiatti[i].ingredienti )==0 || strcmp( ricercaingrediente , p_vettorepiatti[i].ingredienti+20 )==0 || strcmp( ricercaingrediente , p_vettorepiatti[i].ingredienti+40 )==0)
        {
            printf("\n ID=%d", p_vettorepiatti[i].ID);
            printf(" %s ", p_vettorepiatti[i].nome_piatto);
            switch (p_vettorepiatti[i].tipo_piatto)
            {
            case 0:
                printf("antipasto");
                break;
            case 1:
                printf("primo");
                break;
            case 2:
                printf("secondo");
                break;
            case 3:
                printf("contorno");
                break;
            case 4:
                printf("dessert");
                break;
            }
            printf(" %s, %s, %s ", p_vettorepiatti[i].ingredienti, p_vettorepiatti[i].ingredienti+20, p_vettorepiatti[i].ingredienti+40);
            printf("%.2f", p_vettorepiatti[i].prezzo);
            flagricerca=1;
        }
    if (flagricerca==0)
        printf("\n\n Nessun elemento trovato! \n\n");


}

int scegli_tavolo(int f_num_tavoli_ora, conto* p_vettorescontrini)
{

    int k;
    int scelta_tavolo;
    if(f_num_tavoli_ora>0)
    {

        printf("\n\n Tavoli aperti: \n\n");
        for(k=0; k<f_num_tavoli_ora; k++)
            printf("\t%d", p_vettorescontrini[k].ID_numtavolo);
    }
    do
    {
        printf("\n\n\n\n Scegli il numero del tavolo: ");
        scanf("%d", &scelta_tavolo);
        fflush(stdin);
        if (scelta_tavolo<MINIMOTAVOLI || scelta_tavolo>MASSIMOTAVOLI)  //controllo che la quantit� non superi il massimo consetitio o non sia negativa o 0
            scelta_errata();
    }
    while(scelta_tavolo<MINIMOTAVOLI || scelta_tavolo>MASSIMOTAVOLI);
    return scelta_tavolo;
}

int inserisci_qnt_e_controllo()
{
    int quantita;

    do
    {
        printf("\n Inserisci quantita: ");
        scanf("%d", &quantita);
        fflush(stdin);

        if(quantita<MINIMOQNT || quantita>MASSIMOQNT)
            scelta_errata();
    }
    while(quantita<MINIMOQNT || quantita>MASSIMOQNT);

    return quantita;
}


void memorizz_piatto_ordinato(int piatto_selezionato_indice, int ordine_indice, piatto* p_vettorepiatti, piatto* vettoreordini)
{

    // int j;
    //for(j=0; j<p_totale_piatti; j++)
    //{
    //if (p_vettorepiatti[j].ID==id_piatto)     //Facendo scorrere il vettore piatti seleziono quello scelto
    //{
    vettoreordini[ordine_indice]=p_vettorepiatti[piatto_selezionato_indice];      // e lo inserisco nel vettoreordini
    //  }
    //}

}

void aggiorna_vettori_ordine(conto *p_vettorescontrini, int indice_vettorescontrini, piatto* p_vettoreordini, relazionemm *p_vettorerelazionemm, int *p_vettorequantita, int quantita, int contatoretotaleordini)
{


    /*A questo punto aggiorno tutte le informazioni relative a quest'ordine nei vari vettori... */
    p_vettorescontrini[indice_vettorescontrini].ordini=p_vettoreordini;  // L'ordine vero e proprio viene memorizzato nel vettore scontrini, utilizzando la variabile "progressivotavoli" come indice

    p_vettorerelazionemm[indice_vettorescontrini].contatoreordini++;
    printf("\n\n Il contatore ordini relativo al tavolo e' a: %d", p_vettorerelazionemm[indice_vettorescontrini].contatoreordini );
    /* Viene aggiornato il vettore quantit� che ha le quantit� di tutti gli ordini di tutti i tavoli */

    //p_vettorequantita[contatoretotaleordini*3-3]=p_vettorescontrini[progressivotavoli].ID_numtavolo;  // ogni tre spazi abbiamo: ID tavolo
//printf("tavolo: %d", vettorequantita[contatoretotaleordini*3-3]);

    //p_vettorequantita[contatoretotaleordini*3-2]=p_vettorescontrini[progressivotavoli].ordini[0].ID;    // ID ordine
//printf("piatto: %d", vettorequantita[contatoretotaleordini*3-2]);

    //p_vettorequantita[contatoretotaleordini*3-1]=quantita;                                            // quantit� piatto
//printf("qnt: %d", vettorequantita[contatoretotaleordini*3-1]);

    printf("\n il contatore totale ordini e' a: %d \n ", contatoretotaleordini);        // Il contatore totale ordini informa l'utilizzatore sul numero tot di ordini inseriti

}

void aggiorna_qnt_piatto_ordinato_prima_volta(int indice_ordine, int *p_vettorequantita, conto *p_vettorescontrini, piatto* p_vettoreordini, int indice_vettore_scontrini, int quantita, int contatoretotaleordini)
{
    p_vettorequantita[contatoretotaleordini*3-3]=p_vettorescontrini[indice_vettore_scontrini].ID_numtavolo;  // ogni tre spazi abbiamo: ID tavolo
    //printf("tavolo: %d", vettorequantita[contatoretotaleordini*3-3]);

    if (indice_ordine==0)

        p_vettorequantita[contatoretotaleordini*3-2]=p_vettorescontrini[indice_vettore_scontrini].ordini[indice_ordine].ID;    // ID ordine
    //printf("piatto: %d", vettorequantita[contatoretotaleordini*3-2]);
    else

        p_vettorequantita[contatoretotaleordini*3-2]=p_vettoreordini[indice_ordine].ID;

    p_vettorequantita[contatoretotaleordini*3-1]=quantita;                                            // quantit� piatto
    //printf("qnt: %d", vettorequantita[contatoretotaleordini*3-1]);

}



int aggiorna_qnt_piatto_gia_ordinato(int *p_vettorequantita, int scelta_tavolo, int id_piatto,int  contatoretotaleordini, int quantita)
{



    int flag=0;
    int b;

    for(b=0; b<3*contatoretotaleordini; b=b+3)
    {

        if (p_vettorequantita[b]==scelta_tavolo && p_vettorequantita[b+1]==id_piatto)   // Controllo necessario per selezionare la voce del vettore corretta, cio� quella che ha in un campo 0 mod3 l'id del tavolo e nel campo 1 mod 3 l'id del piatto
        {

            p_vettorequantita[b+2]=p_vettorequantita[b+2]+quantita;                 // se lo trova significa che sto aggiungendo un ordine di un piatto che era gi� stato ordinato da quel tavolo quindi devo solo aggiornare la quantit�


            flag=1;         // memorizzo il fatto che l'ultimo piatto inserito era gi� stato ordinato dallo stesso tavolo




        }
    }
    return flag;
}


void stampa_ordini_per_tavolo(int tavolo_selezionato_indice, relazionemm *vettorerelazionemm, conto *p_vettorescontrini, int contatoretotaleordini, int *vettorequantita )
{
    int j;
    int v;

    for(j=0; j<vettorerelazionemm[tavolo_selezionato_indice].contatoreordini; j++)  // faccio scorrere il contatore ordini relativo al tavolo selezionato per stampare gli ordini caricati
    {
        printf("\n ID=%d", p_vettorescontrini[tavolo_selezionato_indice].ordini[j].ID);       // ID di ciascun ordine viene stampato)

        for(v=0; v< contatoretotaleordini*3; v=v+3)                 // ciclo che scorre il totale degli ordini nel vettore quantita che � unico.
        {
            if(vettorequantita[v]==p_vettorescontrini[tavolo_selezionato_indice].ID_numtavolo)        // selezione nel vettore quantit� dell'id di tavolo corretto
            {

                if(vettorequantita[v+1]==p_vettorescontrini[tavolo_selezionato_indice].ordini[j].ID)      // selezione nel vettore quantita dell'id piatto corretto.
                    printf(" qnt=%d", vettorequantita[v+2]);                    // a questo punto la corrispondenza � univoca, un id tavolo & un id piatto possono coincidere con un'unica ordinazione, quindi trovo la corretta quantit� ordinata.
            }
        }




        printf(" %s ", p_vettorescontrini[tavolo_selezionato_indice].ordini[j].nome_piatto);          // questa stampa � diversa dalle altre perch� non stampa tutto il menu ma solo i piatti ordinati dal tavolo
        switch (p_vettorescontrini[tavolo_selezionato_indice].ordini[j].tipo_piatto)
        {
        case 0:
            printf("antipasto");
            break;
        case 1:
            printf("primo");
            break;
        case 2:
            printf("secondo");
            break;
        case 3:
            printf("contorno");
            break;
        case 4:
            printf("dessert");
            break;
        }
        printf(" %s, %s, %s ", p_vettorescontrini[tavolo_selezionato_indice].ordini[j].ingredienti, p_vettorescontrini[tavolo_selezionato_indice].ordini[j].ingredienti+20, p_vettorescontrini[tavolo_selezionato_indice].ordini[j].ingredienti+40);
        printf("%.2f", p_vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo);


    }

}


int intestazione_scontrino (conto *p_vettorescontrini, int tavolo_selezionato_indice, int contatorericevute, int scelta_tavolo)

{
    FILE* fpscontrino = NULL;

    p_vettorescontrini[tavolo_selezionato_indice].tot=0;  // inizializzo il totale a 0
    fpscontrino = fopen ( "scontrino.txt" , "w" ) ;     // apertura file scontrino in scrittutra ( se necessario sovrascrive)
    fprintf(fpscontrino, "Ricevuta n %d del tavolo %d \n\n\n\n", contatorericevute, scelta_tavolo); // contatorericevute � una variabili che viene incrementate opportunamente fungendo da contatore
    fclose(fpscontrino);

    return (contatorericevute+1);
}


int verifica_presenta_tipo_pietanze (relazionemm *vettorerelazionemm, int tavolo_selezionato_indice, conto *p_vettorescontrini, int tipo_pietanza)
{
    int j;
    int flagscontrino=-1;
    for(j=0; j<vettorerelazionemm[tavolo_selezionato_indice].contatoreordini; j++) // facendo scorrere gli ordini del tavolo selezionato identifico se ci sono antipasti (tipo 0) al fine di stamparli sotto la voce "antipasti" dello scontrino
    {
        if (p_vettorescontrini[tavolo_selezionato_indice].ordini[j].tipo_piatto==tipo_pietanza)

        {
            flagscontrino=tipo_pietanza;        // flag per sapere se nel contp ho almeno un antipasto oppure no e stampare oppure no la voce "antipasti"
            break;
        }

    }
    return (flagscontrino);

}

void stampa_pietanze_scontrino (int flagscontrino, relazionemm* vettorerelazionemm, int tavolo_selezionato_indice, int contatoretotaleordini, int *vettorequantita, conto *vettorescontrini, int totale_piatti, piatto *vettorepiatti, int *flagscorte)
{
    FILE* fpscontrino = NULL;
    int len;
    int t;
    int j;
    int v;
    char *strstampa;
    int controlloscorte;

    fpscontrino = fopen ( "scontrino.txt" , "a" ) ;     // modalit� accodamento per mantenere l'intestazione dello scontrino appena scritta

    switch(flagscontrino)
    {
    case 0:
        fprintf(fpscontrino,"\n\nAntipasti\n"); // stampo la dicitura della tipologia
        break;
    case 1:
        fprintf(fpscontrino,"\n\nPrimi\n");
        break;
    case 2:
        fprintf(fpscontrino,"\n\nSecondi\n");
        break;
    case 3:
        fprintf(fpscontrino,"\n\nContorni\n");
        break;
    case 4:
        fprintf(fpscontrino,"\n\nDessert\n");
        break;
    }


    for(j=0; j<vettorerelazionemm[tavolo_selezionato_indice].contatoreordini; j++) // e faccio scorrere, selezionando le ordinazioni che sono "antipasti"
    {
        if (vettorescontrini[tavolo_selezionato_indice].ordini[j].tipo_piatto==flagscontrino)
        {



            for(v=0; v< contatoretotaleordini*3; v=v+3)     // quindi vado a recpuerare la quantit� dal vettore quantit�
            {
                if(vettorequantita[v]==vettorescontrini[tavolo_selezionato_indice].ID_numtavolo)
                {

                    if(vettorequantita[v+1]==vettorescontrini[tavolo_selezionato_indice].ordini[j].ID)
                    {
                        /* per la stampa incolonnata � necessaria questa scala di if in base al numero di cifre della quantit�*/
                        if(vettorequantita[v+2]<10)
                            fprintf(fpscontrino,"\n -   %d x    ", vettorequantita[v+2]);
                        else
                        {
                            if(vettorequantita[v+2]<100)
                                fprintf(fpscontrino,"\n -  %d x    ", vettorequantita[v+2]);
                            else
                            {
                                if(vettorequantita[v+2]<1000)
                                    fprintf(fpscontrino,"\n - %d x    ", vettorequantita[v+2]);
                            }
                        }


                        strstampa=(char*) calloc (30 , sizeof(char));       // in base alla lunghezza della stinga della pietanza devo inserire un numero di spazi adeguato a incolonnare
                        len = strlen (vettorescontrini[tavolo_selezionato_indice].ordini[j].nome_piatto);
                        // printf(" lunghezza: %d", len);
                        for(t=0; t<30-len; t++)
                        {
                            strstampa[t]=' ';

                        }
                        //printf(" caratteri riemp %d", t);
                        fprintf(fpscontrino," %s ", vettorescontrini[tavolo_selezionato_indice].ordini[j].nome_piatto);
                        //printf("\n controlloscorte%d ", controlloscorte);
                        controlloscorte=ricerca_piatto_scontrino_menu(vettorescontrini, vettorepiatti, totale_piatti, tavolo_selezionato_indice, j);
                       //printf("\n controlloscorte%d ", controlloscorte);
                       if ( controlloscorte>-1)
                            memorizza_venduto_scorta(vettorepiatti, controlloscorte, flagscorte, vettorequantita[v+2]);





                        fprintf(fpscontrino," %s ", strstampa);


                        free(strstampa);
                        strstampa=NULL;



                        if((vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2])<10)      /* per la stampa incolonnata � necessaria questa scala di if in base al numero di cifre della quantit�*/
                            fprintf(fpscontrino,"    %.2f   euro", vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2]);


                        if((vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2])>=10 && (vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2])<100)
                                fprintf(fpscontrino,"   %.2f   euro", vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2]);


                        if((vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2])>=100 && (vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2])<1000)
                                    fprintf(fpscontrino,"  %.2f   euro", vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2]);

                        if((vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2])>=1000)
                                        fprintf(fpscontrino," %.2f   euro", vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo*vettorequantita[v+2]);



                        //printf( "\nqnt=%d\n", vettorequantita[v+2]);        // stampa a schermo della quantita su scontrino
                        vettorescontrini[tavolo_selezionato_indice].tot=vettorescontrini[tavolo_selezionato_indice].tot+(vettorescontrini[tavolo_selezionato_indice].ordini[j].prezzo)*vettorequantita[v+2];
                        //printf("\ntotparz%.2f", vettorescontrini[tavolo_selezionato_indice].tot);   // stampa a schermo totale costo per singola pietanza ( se ho 2 unit� della stessa pietanza qui avr� il costo complessivo delle due unit�)

                    }
                }
            }
        }
    }
    fclose(fpscontrino);

}



void stampa_totale_e_sconto(conto *vettorescontrini, int tavolo_selezionato_indice, float *totalescontato, float *fatturatogg)

{
    FILE* fpscontrino = NULL;

    /*Totale e sconto*/
    fpscontrino = fopen ( "scontrino.txt" , "a" ) ;

    if (vettorescontrini[tavolo_selezionato_indice].tot<10)     // gestione degli spazi per l'incolonnamento del totale scontrino
    {


        fprintf(fpscontrino, "\n\n\n Totale:                                         ");
        fprintf(fpscontrino," %.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot);
    }
    else
    {
        if (vettorescontrini[tavolo_selezionato_indice].tot<100)
        {
            fprintf(fpscontrino, "\n\n\n Totale:                                        ");
            fprintf(fpscontrino," %.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot);
            if (vettorescontrini[tavolo_selezionato_indice].tot>40)
            {
                fprintf(fpscontrino, "\n\n Sconto:                                          ");
                fprintf(fpscontrino, "%.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot/10);
            }


        }
        else
        {
            if (vettorescontrini[tavolo_selezionato_indice].tot<1000)
            {
                fprintf(fpscontrino, "\n\n\n Totale:                                       ");
                fprintf(fpscontrino," %.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot);
                fprintf(fpscontrino, "\n\n Sconto:                                         ");
                fprintf(fpscontrino, "%.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot/10);



            }
            else
            {
                if (vettorescontrini[tavolo_selezionato_indice].tot<10000)
                {

                    fprintf(fpscontrino, "\n\n\n Totale:                                      ");
                    fprintf(fpscontrino," %.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot);
                    fprintf(fpscontrino, "\n\n Sconto:                                        ");
                    fprintf(fpscontrino, "%.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot/10);
                }
                else
                {
                    if (vettorescontrini[tavolo_selezionato_indice].tot<100000)
                    {

                        fprintf(fpscontrino, "\n\n\n Totale:                                     ");
                        fprintf(fpscontrino," %.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot);

                        fprintf(fpscontrino, "\n\n Sconto:                                       ");
                        fprintf(fpscontrino, "%.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot/10);
                    }
                }
            }
        }

    }
    if (vettorescontrini[tavolo_selezionato_indice].tot>40) // gestione sconto
    {

        (*totalescontato)=vettorescontrini[tavolo_selezionato_indice].tot-vettorescontrini[tavolo_selezionato_indice].tot/10;
        if ((*totalescontato)<100)
            fprintf(fpscontrino, "\n\n\n Totale scontato:                                ");
        else if ((*totalescontato)<1000)
            fprintf(fpscontrino, "\n\n\n Totale scontato:                               ");
        else if ((*totalescontato)<10000)
            fprintf(fpscontrino, "\n\n\n Totale scontato:                              ");
        else if ((*totalescontato)<100000)
            fprintf(fpscontrino, "\n\n\n Totale scontato:                             ");


        fprintf(fpscontrino, "%.2f   euro", (*totalescontato));
        (*fatturatogg)+=(*totalescontato);

    }
    else
        (*fatturatogg)+=vettorescontrini[tavolo_selezionato_indice].tot;


    //printf("\n\nfatturatoogg=%f\n\n", *fatturatogg);



    fclose(fpscontrino);
}


void fatturatototale_e_fatturatogiornaliero(float *fatturatogg, float *fatturatotot, conto *vettorescontrini, int tavolo_selezionato_indice, float totalescontato)

{

    FILE* fpfatturatogg = NULL;
    FILE* fpfatturatotot = NULL;


    fpfatturatogg = fopen ( "fatturatogg.txt" , "w" ) ;
    fprintf(fpfatturatogg, "%.2f   euro", *fatturatogg);
    fclose(fpfatturatogg);

    //printf("%.2f   euro", *fatturatotot);

    // if (fpfatturatotot==NULL)
    //{

    fpfatturatotot = fopen ( "fatturatotot.txt" , "r" ) ;       // gestione del file sul totale complessivo sino a cancellazione del file stesso
    fscanf(fpfatturatotot, "%f", fatturatotot);
    //printf("letto da file %.2f   euro", fatturatotot);

    fclose(fpfatturatotot);

    fpfatturatotot = fopen ( "fatturatotot.txt" , "w" );
    if (vettorescontrini[tavolo_selezionato_indice].tot>40)
    {

        (*fatturatotot)+=totalescontato;
        // printf("\n\n>40 totscontato%.2f   euro", totalescontato);
        //printf("\n\n>40 fatttot%.2f   euro", fatturatotot);
    }
    else
    {


        (*fatturatotot)+=vettorescontrini[tavolo_selezionato_indice].tot;
        //printf("\n\n <40 totscontr%.2f   euro", vettorescontrini[tavolo_selezionato_indice].tot);
        //printf("\n\n <40 fatttot%.2f   euro", fatturatotot);
    }
    fprintf(fpfatturatotot, "%.2f   euro", *fatturatotot);

    // printf("%.2f   euro", fatturatotot);


    //}

    /* else
     {
         if (vettorescontrini[tavolo_selezionato_indice].tot>40)
             fatturatotot+=totalescontato;
         else
             fatturatotot+=vettorescontrini[tavolo_selezionato_indice].tot;
         fpfatturatotot = fopen ( "fatturatotot.txt" , "w" ) ;
         fprintf(fpfatturatotot, "%.2f   euro", fatturatotot);
     }*/
    fclose(fpfatturatotot);




}



void aggiunta_scorte(piatto* vettorepiatti, int numero_totale_piatti, int* flagscorte)

{
    FILE* fpscorte = NULL;
    FILE* fpscortesupp = NULL;
    //int flagscorte[2];
    int len;
    int i;
    int x;
    int a;
    char stringacontrolloscorte[20];
    scorte *vettorescorte=NULL;
    scorte *vettorescortebackup=NULL;
    int ingredientedoppio1;
    int ingredientedoppio2;
    int ingredientedoppio3;
    int flagdoppione=0;




    fpscortesupp = fopen ("scortesupp.dat", "rb" );
    fread(flagscorte , sizeof ( int ) , 2, fpscortesupp ) ;

    fclose(fpscortesupp);
    flagscorte[1]=flagscorte[1]+3;





    if (flagscorte[0]==0)
    {


        vettorescorte = (scorte*) malloc (flagscorte[1]*(sizeof(scorte)));
        //vettorescortebackup = (scorte*) malloc (flagscorte[1]*(sizeof(scorte)));
        flagscorte[0]=1;
        fpscortesupp = fopen ("scortesupp.dat", "wb" );
        fwrite(flagscorte , sizeof ( int ) , 2, fpscortesupp ) ;
        fclose(fpscortesupp);


        for(x=0; x<20; x++)
        {
            vettorescorte[0].ingrediente[x]=' ';
            vettorescorte[1].ingrediente[x]=' ';
            vettorescorte[2].ingrediente[x]=' ';

        }


        strcpy(vettorescorte[0].ingrediente,vettorepiatti[(numero_totale_piatti-1)].ingredienti);
        vettorescorte[0].venduto=0;
        strcpy(vettorescorte[1].ingrediente,vettorepiatti[(numero_totale_piatti-1)].ingredienti+20);
        vettorescorte[1].venduto=0;
        strcpy(vettorescorte[2].ingrediente,vettorepiatti[(numero_totale_piatti-1)].ingredienti+40);
        vettorescorte[2].venduto=0;

        fpscorte = fopen ("scorte.dat", "wb" );
        fwrite(vettorescorte , sizeof ( scorte ) , flagscorte[1], fpscorte) ;

        fclose(fpscorte);

    }
    else
    {

        /*vettorescortebackup=vettorescorte;
        free(vettorescorte);
        vettorescorte = (scorte*) malloc (flagscorte[1]*(sizeof(scorte)));
        vettorescorte=vettorescortebackup;
        free(vettorescortebackup);*/

        vettorescorte=(scorte*) realloc (vettorescorte,(flagscorte[1])*(sizeof(scorte)));

        fpscorte = fopen ("scorte.dat", "rb" );
        fread(vettorescorte , sizeof ( scorte ) , flagscorte[1], fpscorte) ;

        fclose(fpscorte);

        for(i=0; i<flagscorte[1]; i++)
            for(x=0; x<60; x+=20)
                if(strcmp(vettorescorte[i].ingrediente, vettorepiatti[(numero_totale_piatti-1)].ingredienti+x)==0)
                {
                    if (x==0)
                        ingredientedoppio1=x;
                    if (x==20)
                        ingredientedoppio2=x;
                    if (x==40)
                        ingredientedoppio3=x;
                }


        for(x=0; x<20; x++)
        {
            vettorescorte[flagscorte[1]-3].ingrediente[x]=' ';
            vettorescorte[flagscorte[1]-2].ingrediente[x]=' ';
            vettorescorte[flagscorte[1]-1].ingrediente[x]=' ';

        }

        //for(i=0; i<flagscorte[1]; i++)
            //printf("\t %s %d\n", vettorescorte[i].ingrediente, vettorescorte[i].venduto);



            if (ingredientedoppio1!=0)
            {
                strcpy(vettorescorte[flagscorte[1]-3].ingrediente, vettorepiatti[(numero_totale_piatti-1)].ingredienti);
                a=strlen(vettorepiatti[(numero_totale_piatti-1)].ingredienti);

                //printf("%s", vettorescorte[flagscorte[1]-3].ingrediente);
                vettorescorte[flagscorte[1]-3].venduto=0;
                flagdoppione=0;
            }
            else
            {
                flagdoppione=1;
                flagscorte[1]--;
            }

        if (ingredientedoppio2!=20)
        {
            strcpy(vettorescorte[flagscorte[1]-2].ingrediente, vettorepiatti[(numero_totale_piatti-1)].ingredienti+20);
            a=strlen(vettorepiatti[(numero_totale_piatti-1)].ingredienti+20);

            //printf("%s", vettorescorte[flagscorte[1]-2].ingrediente);
            vettorescorte[flagscorte[1]-2].venduto=0;
            flagdoppione=0;
        }
        else
        {
            flagdoppione=1;
            flagscorte[1]--;
        }

        if (ingredientedoppio3!=40)
        {


            strcpy(vettorescorte[flagscorte[1]-1].ingrediente, vettorepiatti[(numero_totale_piatti-1)].ingredienti+40);
            a=strlen(vettorepiatti[(numero_totale_piatti-1)].ingredienti+40);

            //printf("%s", vettorescorte[flagscorte[1]-1].ingrediente);
            vettorescorte[flagscorte[1]-1].venduto=0;
            flagdoppione=0;
        }
        else
        {
            flagdoppione=1;
            flagscorte[1]--;
        }


        fpscorte = fopen ("scorte.dat", "wb" );
        // printf("%d", flagscorte[1]);
        fwrite(vettorescorte , sizeof ( scorte ) , flagscorte[1], fpscorte) ;

        fclose(fpscorte);



        fpscortesupp = fopen ("scortesupp.dat", "wb" );
        fwrite(flagscorte , sizeof ( int ) , 2, fpscortesupp ) ;
        fclose(fpscortesupp);
    }
    //printf("wwwwww");





}


void visualizza_scorte (scorte *vettorescorte,int *flagscorte)
{
    FILE* fpscorte = NULL;
    FILE* fpscortesupp = NULL;
    int i;





    fpscortesupp = fopen ("scortesupp.dat", "rb" );
    fread(flagscorte , sizeof ( int ) , 2, fpscortesupp ) ;

    fclose(fpscortesupp);

    vettorescorte = (scorte*) malloc (flagscorte[1]*(sizeof(scorte)));

    fpscorte = fopen ("scorte.dat", "rb" );
    fread(vettorescorte , sizeof ( scorte ) , flagscorte[1], fpscorte) ;

    fclose(fpscorte);

        printf("\n\n SCORTE E NUMERO UTILIZZI\n\n");
    for(i=0; i<flagscorte[1]; i++)
        printf("\t%s %d\n", vettorescorte[i].ingrediente, vettorescorte[i].venduto);

    free(vettorescorte);


}




int ricerca_piatto_scontrino_menu(conto *vettorescontrini, piatto *p_vettorepiatti, int p_totale_piatti, int tavolo_selezionato_indice, int j)
{
                         int i=0;
    //printf(" %s ", p_vettorepiatti[(p_totale_piatti-1)].nome_piatto);
    for(i=0; i<p_totale_piatti; i++)
    {

        //printf("\n cmp: %s %s ", vettorescontrini[tavolo_selezionato_indice].ordini[j].nome_piatto, p_vettorepiatti[i].nome_piatto);
        if(strcmp(vettorescontrini[tavolo_selezionato_indice].ordini[j].nome_piatto, p_vettorepiatti[i].nome_piatto)==0)
        {//printf("%d", i);
            return i;}

    }
    return -1;
}

void memorizza_venduto_scorta(piatto *vettorepiatti, int j, int *flagscorte, int qnt)
{
int q;
int i;
scorte *vettorescorte=NULL;

 FILE* fpscorte = NULL;
 FILE* fpscortesupp = NULL;

 //printf("siamo dentro memorizza venduto scorte");


    fpscortesupp = fopen ("scortesupp.dat", "rb" );
    fread(flagscorte , sizeof ( int ) , 2, fpscortesupp ) ;

    fclose(fpscortesupp);

    vettorescorte = (scorte*) malloc (flagscorte[1]*(sizeof(scorte)));

    fpscorte = fopen ("scorte.dat", "rb" );
    fread(vettorescorte , sizeof ( scorte ) , flagscorte[1], fpscorte) ;

    fclose(fpscorte);

for(i=0; i<flagscorte[1]; i++)
{
for (q=0; q<60; q+=20)
{
//printf("\nstrcmp%d" , strcmp(vettorepiatti[j].ingredienti+q, vettorescorte[i].ingrediente));
 if(strcmp(vettorepiatti[j].ingredienti+q, vettorescorte[i].ingrediente)==0)
 {

    vettorescorte[i].venduto=vettorescorte[i].venduto+1*qnt;
    //printf("\t %s %d\n", vettorescorte[i].ingrediente, vettorescorte[i].venduto);
 }
}
}
//for(i=0; i<flagscorte[1]; i++)
  //      printf("\t %s %d\n", vettorescorte[i].ingrediente, vettorescorte[i].venduto);

fpscorte = fopen ("scorte.dat", "wb" );
    fwrite(vettorescorte , sizeof ( scorte ) , flagscorte[1], fpscorte) ;

    fclose(fpscorte);



}








