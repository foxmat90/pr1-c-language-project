#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MINIMOQNT 1
#define MASSIMOQNT 1000
#define MINIMOTAVOLI 1
#define MASSIMOTAVOLI 1000



 typedef enum { Antipasto,  Primo,  Secondo,  Contorno,  Dessert} tipologia;//Enumerazione tipologia piatti come da specifiche

    //Struttura per i piatti
typedef struct
    {
        int ID;
        char nome_piatto [30] ;
        tipologia tipo_piatto;
        char ingredienti[60] ;
        float prezzo;
    } piatto ;

    //struttura conto per ogni tavolo e per la stampa dello scontrino

    typedef struct
    {
        int ID_numtavolo;
        int ID_progressivo;
        piatto *ordini;
        float tot;
    } conto;

    typedef struct
    {
        int ID_numtavolo;
        int ID_progressivo;
        int contatoreordini;

    } relazionemm;

    typedef struct
    {
        char ingrediente[20];
        int venduto;
    }  scorte;


void scelta_errata(void);
int menu_principale(void);
void carica_file_supporto(int*, int*, int*);
void carica_file_menu(piatto*, int);
void riempi_ingredienti(piatto*, int);
int controllo_doppioni(piatto*, int);
void inserimento_campo_piatto(piatto*, int, int);
void memorizza_file_menu(piatto*, int);
void memorizza_file_supporto(int*);
void stampa_menu(piatto*, int);
int gestione_cassa (void);
int selezione_piatto_e_controllo(int*, int, int, piatto*);
void compattazione_piatti_dopo_eliminazione(int, int, int, int, piatto*);
int modifica_piatti(int, int, int, int, piatto*);
void ricerca_per_ingrediente(int, piatto*);
int scegli_tavolo(int, conto*);
int inserisci_qnt_e_controllo();
void memorizz_piatto_ordinato(int, int, piatto*, piatto*);
void aggiorna_vettori_ordine(conto*, int, piatto*, relazionemm*, int*, int, int);
void aggiorna_qnt_piatto_ordinato_prima_volta(int, int*, conto*, piatto*, int, int, int);
int aggiorna_qnt_piatto_gia_ordinato( int*, int, int, int, int);
void stampa_ordini_per_tavolo(int, relazionemm*, conto*, int, int* );
int intestazione_scontrino (conto*, int, int, int);
int verifica_presenta_tipo_pietanze (relazionemm*, int, conto*, int);
void stampa_pietanze_scontrino (int , relazionemm* , int, int, int*, conto*, int, piatto*, int*);
void stampa_totale_e_sconto(conto* , int , float*, float*);
void fatturatototale_e_fatturatogiornaliero(float*, float*, conto*, int , float);
void aggiunta_scorte(piatto*, int, int*);
void visualizza_scorte (scorte*,int*);
int ricerca_piatto_scontrino_menu(conto*, piatto*, int, int, int);
void memorizza_venduto_scorta(piatto*, int, int*, int);













